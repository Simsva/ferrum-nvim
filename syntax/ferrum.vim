if exists("b:current_syntax")
  finish
endif

" Statements
syn keyword feStatement     return break continue
syn keyword feConditional   if else
syn keyword feRepeat        while for do

" Other keywords
syn keyword feType          int string void

" Comments
syn keyword feTodo          contained TODO FIXME
syn cluster feCommentGroup  contains=feTodo

syn region feCommentL start="//" end="$"    keepend contains=@feCommentGroup
syn region feComment  start="/\*" end="\*/" contains=@feCommentGroup

" Strings
syn region feString start=+"+ end=+"+ end='$' extend

" Numbers
syn match feNumbers display transparent "\<\d" contains=feNumber
syn match feNumber  display contained "\d\+\>"

let b:current_syntax = "ferrum"
hi def link feCommentL      feComment

hi def link feStatement     Statement
hi def link feConditional   Conditional
hi def link feRepeat        Repeat
hi def link feType          Type
hi def link feTodo          Todo
hi def link feComment       Comment
hi def link feString        String
hi def link feNumber        Number
