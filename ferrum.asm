BITS 64
str_0 db "Hello world", 0xA, 0xD
str_0_len equ $-str_0
section .text
global _start:
_start:
call main
mov edi, eax
mov eax, 60
syscall

put:
mov rsi, [rsp + 0x10]
mov rdx, [rsp + 0x08]
mov rax, 1
mov rdi, 1
syscall
ret

global main
main:
push rbp
mov rbp, rsp
sub rsp, 0
push r8
mov r8, rsp
push str_0
push str_0_len
call put
mov rsp, r8
pop r8
mov rax, 0
push rax
pop rax
jmp .main_end
.main_end:
mov rsp, rbp
pop rbp
ret
