# ferrum-nvim

Simple syntax file for the [Ferrum](https://github.com/YayL/Ferrum) programming language by [YayL](https://github.com/YayL)
